/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';
// lista de menu felixab22
const menu: FuseNavigationItem[] = [
    {
        id: 'dashboards',
        title: 'ACTAS',
        subtitle: 'Actas de las reuniones',
        type: 'group',
        icon: 'heroicons_outline:home',
        children: [
            {
                id: 'project.project',
                title: 'ACTAS FIRMADAS',
                type: 'basic',
                icon: 'heroicons_outline:clipboard-check',
                link: '/example',
            },
            {
                id: 'analytics.analytics',
                title: 'CONFIGURACIÓN',
                type: 'collapsable',
                icon: 'heroicons_outline:check-circle',
                children: [
                    {
                        id: 'analytics.analytics2',
                        title: 'INCIDENCIAS',
                        type: 'basic',
                        icon: 'heroicons_outline:check-circle',
                        link: '/example/Listar',
                    },
                    {
                        id: 'analytics.analytics3',
                        title: 'USUARIOS',
                        type: 'basic',
                        icon: 'heroicons_outline:check-circle',
                        link: '/example/Usuario',
                    },
                ],
            },
            {
                id: 'apps.tasks',
                title: 'REPORTE',
                type: 'basic',
                icon: 'heroicons_outline:chart-pie',
                link: '/example/Grafica',
            },
        ],
    },
];

export const compactNavigation: FuseNavigationItem[] = menu;
export const futuristicNavigation: FuseNavigationItem[] = menu;
export const horizontalNavigation: FuseNavigationItem[] = menu;
export const defaultNavigation: FuseNavigationItem[] = menu;
