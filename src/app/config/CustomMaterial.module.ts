import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {MatAutocompleteModule} from '@angular/material/autocomplete';

import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';


import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
    declarations: [],
        imports: [
      CommonModule,
      MatToolbarModule,
      MatButtonModule,
      MatCardModule,
      MatInputModule,
      MatDialogModule,
      MatTableModule,
      MatMenuModule,
      // MatIconModule,
      MatProgressSpinnerModule,
      DragDropModule,
      FormsModule,
      RouterModule,
      MatFormFieldModule,
      MatDatepickerModule,
      // NativeDateAdapter,
      MatNativeDateModule,
      MatRippleModule,
      MatPaginatorModule,
      MatTabsModule,
      MatExpansionModule,
      MatRadioModule,
      MatMomentDateModule,
      MatIconModule,
      MatSelectModule,
      MatStepperModule,
      MatCheckboxModule,
      MatChipsModule,
      MatListModule,
      MatProgressBarModule,
      MatSidenavModule,
      MatTooltipModule,
      MatAutocompleteModule
    ],
    exports: [
      MatMomentDateModule,
      MatButtonModule,
      // MatIconModule,
      CommonModule,
      MatToolbarModule,
      MatCardModule,
      MatInputModule,
      MatDialogModule,
      MatTableModule,
      MatMenuModule,
      MatIconModule,
      MatProgressSpinnerModule,
      DragDropModule,
      FormsModule,
      RouterModule,
      MatFormFieldModule,
      MatDatepickerModule,
      // NativeDateAdapter,
      MatNativeDateModule,
      MatRippleModule,
      MatPaginatorModule,
      MatTabsModule,
      MatExpansionModule,
      MatRadioModule,
      MatCheckboxModule,
      MatChipsModule,
      MatListModule,
      MatProgressBarModule,
      MatSidenavModule,
      MatTooltipModule,
      MatSelectModule,
      MatStepperModule,
      MatAutocompleteModule
    ],
  })
  // eslint-disable-next-line eol-last
  export class CustomMaterialModule {}