import { FormControl, ValidationErrors } from '@angular/forms';

export class ValidacionesPropias {
    static esnumero(control: FormControl): ValidationErrors {
        if ( !isNaN(control.value))
            return null;
        else
            return { esnumero: true }
    }

}