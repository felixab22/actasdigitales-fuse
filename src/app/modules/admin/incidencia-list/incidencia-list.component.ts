/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit, ViewChild,AfterViewInit } from '@angular/core';
import { IncidenciasService } from 'app/service/incidencias.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { IncidenciaModel } from 'app/model/incidencia.model';
@Component({
  selector: 'app-incidencia-list',
  templateUrl: './incidencia-list.component.html',
  styleUrls: ['./incidencia-list.component.scss']
})
export class IncidenciaListComponent implements OnInit {
  displayedColumns: string[] = ['idincidencia', 'descripcion'];
  dataSource: MatTableDataSource<IncidenciaModel>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  done = new Array<IncidenciaModel>();
  constructor(
    private _incidenciaSrv: IncidenciasService
    ) {
      this.dataSource = new MatTableDataSource(this.done);
    }

  ngOnInit(): void {
    this.listarInicidencia();
  }
listarInicidencia(){
  this._incidenciaSrv.getListaIncidencias().subscribe((res: any)=>{
    console.log(res);
    this.done = res.data;
    this.dataSource = new MatTableDataSource(this.done);
  });
}
// eslint-disable-next-line @angular-eslint/use-lifecycle-interface
ngAfterViewInit() {
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
}

applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
}
