/* eslint-disable arrow-parens */
/* eslint-disable no-var */
import { Component, OnInit } from '@angular/core';
import {
    FormGroup,
    FormControl,
    Validators,
    FormBuilder,
    FormArray,
} from '@angular/forms';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
} from '@angular/material/core';
import {
    MAT_MOMENT_DATE_FORMATS,
    MomentDateAdapter,
} from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IncidenciasService } from '../../../service/incidencias.service';
import { map, startWith } from 'rxjs/operators';
import { IncidenciaModel } from 'app/model/incidencia.model';
moment.locale('es');
@Component({
    selector: 'app-incidencias',
    templateUrl: './incidencias.component.html',
    styleUrls: ['./incidencias.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE],
        },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    ],
})
export class IncidenciasComponent implements OnInit {
    dialogTitle = 'REGISTRAR CURSO DE CERTIFICACIÓN';
    validatingForm: FormGroup;
    incidenciaFrom: FormGroup;
    options = new Array<IncidenciaModel>();
    fecha = new Date();
    action = 'new';
    contact: any;
    imagen = './assets/img/pat1.jpg' || 'surveillance/assets/img/pat1.jpg';
    verhoras = true;
    verFecha = false;
    public _data: any;
    categories = [
        { id: 1, slug: 'I', title: 'I' },
        { id: 2, slug: 'II', title: 'II' },
        { id: 3, slug: 'III', title: 'III' },
    ];
    turno = [
        { id: 1, slug: '14:00-22:00', title: '14:00-22:00' },
        { id: 2, slug: '22:00-06:00', title: '22:00-06:00' },
        { id: 3, slug: '06:00-14:00', title: '06:00-14:00' },
    ];
    filteredOptions: Observable<IncidenciaModel[]>;

    constructor(
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _incidenciaSrv: IncidenciasService
    ) {}

    ngOnInit(): void {
        this.newValidatorForm();
        this.listincidencias();
        this.filteredOptions = this.validatingForm
            .get('incidencia')
            .valueChanges.pipe(
                startWith(''),
                map((value) =>
                    typeof value === 'string' ? value : value.descripcion
                ),
                map((descripcion) =>
                    descripcion
                        ? this._filter(descripcion)
                        : this.options.slice()
                )
            );
    }
    buscar(e: KeyboardEvent) {
        const value = e.target['value'];
        if (e.keyCode !== 8) {
            if (value.length > 4) {
                this._incidenciaSrv
                    .getListaParte(value)
                    .subscribe((res: any) => {
                        if (res.code === 204) {
                            //  swal('INFORME!', 'INCIDENCIA NO ENCONTRADA', 'warning');
                        } else if (res.code === 200) {
                            // this.incidencias = res.data;
                            console.log(res);
                        }
                    });
            }
        }
    }

    listincidencias() {
        this._incidenciaSrv.getListaIncidencias().subscribe((res: any) => {
            console.log(res);
            if (res.code === 204) {
            } else if (res.code === 200) {
                this.options = res.data;
            }
        });
    }
    generarDocumento(valor) {
        // const enviar = {valor:valor,accion:'new'};
        console.log(valor);
        var enviar = {
            nroparte: valor.nroparte,
            receptor: valor.receptor,
            incidencia: valor.idincidencia,
            referencia: valor.referencia,
            turno: valor.turno,
            sector: valor.sector,
            contenido: valor.contenido,
            hora: valor.hora,
            fname: valor.fname,
            fregistro: valor.fregistro,
            idpersona: 2,
        };
        this._incidenciaSrv.SaveOrUpdateParte(enviar).subscribe((res: any) => {
            // console.log(res);
            var guarda = {tipo:'nuevo',dato:valor,idparte:res.data.idparte};
            if (res.code === 200) {
                localStorage.setItem('parte', JSON.stringify(guarda));
                this._router.navigate(['/example/Imprimir']);
            }
        });
    }
    selecctionOption(valor) {
        console.log(valor);
        this.validatingForm = this.createVarlidatortForm(valor);
    }
    createVarlidatortForm(valor): FormGroup {
        return this._formBuilder.group({
            nroparte: [valor.nroparte],
            receptor: [valor.receptor],
            incidencia: [valor.incidencia.descripcion],
            idincidencia: [valor.incidencia.idincidencia],
            referencia: [valor.referencia],
            turno: [valor.turno],
            sector: [valor.sector],
            hora: [valor.hora],
            fregistro: [valor.fregistro],
            lugar: [valor.lugar],
            contenido: [valor.contenido],
            fname: [valor.fname],
        });
    }
    newValidatorForm() {
        this.validatingForm = new FormGroup({
            nroparte: new FormControl('', [Validators.required]),
            receptor: new FormControl('', [Validators.required]),
            incidencia: new FormControl('', [Validators.required]),
            idincidencia: new FormControl('', [Validators.required]),
            referencia: new FormControl('', [Validators.required]),
            turno: new FormControl('14:00-22:00', [Validators.required]),
            sector: new FormControl('I', [Validators.required]),
            hora: new FormControl('', [Validators.required]),
            fregistro: new FormControl('', [Validators.required]),
            lugar: new FormControl('', [Validators.required]),
            contenido: new FormControl('', [Validators.required]),
            fname: new FormControl(moment().format('LL')),
        });
    }
    private _filter(name: string): IncidenciaModel[] {
        const filterValue = name.toLowerCase();
        return this.options.filter(
            (option) =>
                option.descripcion.toLowerCase().indexOf(filterValue) === 0
        );
    }
}
