import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';
import { CustomMaterialModule } from 'app/config/CustomMaterial.module';
import { ExampleComponent } from 'app/modules/admin/example/example.component';
import { PipesModule } from 'app/pipe/pipes.module';
import { IncidenciasComponent } from './incidencias/incidencias.component';
import { IncidenciaPrintComponent } from './incidencia-print/incidencia-print.component';
import { FuseFindByKeyPipeModule } from '@fuse/pipes/find-by-key';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { IncidenciaListComponent } from './incidencia-list/incidencia-list.component';
import { GraficaComponent } from './grafica/grafica.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
// import { ChartsModule, ChartSimpleModule, WavesModule } from 'ng-uikit-pro-standard'

const exampleRoutes: Route[] = [
    {
        path     : '',
        component: ExampleComponent
    },
    {
        path     : 'Incidencias',
        component: IncidenciasComponent
    },
    {
        path     : 'Imprimir',
        component: IncidenciaPrintComponent
    },
    {
        path     : 'Listar',
        component: IncidenciaListComponent
    },
    {
        path     : 'Grafica',
        component: GraficaComponent
    },
    {
        path     : 'Usuario',
        component: UsuariosComponent
    }
];

@NgModule({
    declarations: [
        ExampleComponent,
        IncidenciasComponent,
        IncidenciaPrintComponent,
        IncidenciaListComponent,
        GraficaComponent,
        UsuariosComponent
    ],
    imports     : [
        RouterModule.forChild(exampleRoutes),
        CustomMaterialModule,
        ReactiveFormsModule,
        PipesModule,
        FormsModule,
        FuseFindByKeyPipeModule,
        Ng2SearchPipeModule        ]
})
export class ExampleModule
{
}
