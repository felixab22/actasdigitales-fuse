/* eslint-disable no-var */
import { Component, OnInit } from '@angular/core';
import { ReporteModel } from 'app/model/incidencia.model';
import { IncidenciasService } from 'app/service/incidencias.service';
import * as XLSX from 'xlsx';
// import Chart from 'chart.js';

@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.component.html',
  styleUrls: ['./grafica.component.scss']
})
export class GraficaComponent implements OnInit {
  sectores1 = [0];
  sectores2 = [0];
  sectores3 = [0];
  labeles = [''];
  // public chartType: string = 'bar';
  finicio = new Date().toISOString().substr(0, 8) + '01';
  ffin = new Date().toISOString().substr(0, 10);
  listar: any = [
    { data: 2, result: 'Pastel' },
    { data: 2, result: 'Dona' },
  ];

  listaSector = [
    { incidencia: 'ACCIDENTE DE TRANSITO VEHICULAR', sector1: 2, sector2: 3, sector3: 6 }
  ];
  tiposgraficas = [
    { id: 1, descripcion: 'Pastel', tipo: 'pie' },
    { id: 2, descripcion: 'Dona', tipo: 'doughnut' },
    { id: 3, descripcion: 'Barras', tipo: 'bar' },
    { id: 4, descripcion: 'Barras horizontal', tipo: 'horizontalBar' },
    { id: 5, descripcion: 'Polar', tipo: 'polarArea' },
    { id: 6, descripcion: 'Lineal', tipo: 'line' }

  ];
  constructor(private _incidenciaSrv: IncidenciasService) {}
  ngOnInit() {
    this._incidenciaSrv.getChartIncidencias(this.finicio, this.ffin).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {

        this.listar = this.fusionar(res.cantidad, res.incidencia, res.sector);

      }
    });
  }
  graficar() {
    const inicio = this.finicio.substr(0, 10);
    const fin = this.ffin.substr(0, 10);
    this._incidenciaSrv.getChartIncidencias(inicio, fin).subscribe((res: any) => {
      if (res.code === 200) {
        this.listar = this.fusionar(res.cantidad, res.incidencia, res.sector);
      } else if (res.code === 204) {

      }
    });
  }

  fusionar(cantidad, incidencia, sector) {
    const salida = [{ incidencia: null, sector1: null, sector2: null, sector3: null }];
    let j = 0;
    let dobblea = true;
    for (let i = 0; i < cantidad.length; i++) {
      j = salida.length -1;
      var sect = sector[i];
      if(i === 0) {
      if (sect === 'I') {
        salida[0].incidencia = incidencia[0];
        salida[0].sector1 = cantidad[0];
        salida[0].sector2 = 0;
        salida[0].sector3 = 0;
        console.log('entro-I-'+i);
      }
      if (sect === 'II') {
        salida[0].incidencia = incidencia[0];
        salida[0].sector1 = 0;
        salida[0].sector2 = cantidad[0];
        salida[0].sector3 = 0;
        console.log('entro-II-'+i);
      }
      if (sect === 'III') {
        salida[0].incidencia = incidencia[0];
        salida[0].sector1 = 0;
        salida[0].sector2 = 0;
        salida[0].sector3 = cantidad[0];
        console.log('entro-III-'+i);
      } }
      if (i !== 0) {
        console.log('j='+j+' & i='+i);
        if(incidencia[j] === incidencia[i]){
          if (sect === 'II' && dobblea === true) {
            salida[j].sector2 = cantidad[i];
            dobblea = false;
          }else  if (sect === 'III') {
            salida[j].sector3 = cantidad[i];
            console.log('entro-III-'+i);
        } else {
          dobblea = true;
           j++;
          const subida = { incidencia: null, sector1: null, sector2: null, sector3: null };
          if (sect === 'I') {
            subida.incidencia = incidencia[i];
            subida.sector1 = cantidad[i];
            subida.sector2 = 0;
            subida.sector3 = 0;
          } else if (sect === 'II') {
            subida.incidencia = incidencia[i];
            subida.sector1 = 0;
            subida.sector2 = cantidad[i];
            subida.sector3 = 0;
          } else if (sect === 'III') {
            subida.incidencia = incidencia[i];
            subida.sector1 = 0;
            subida.sector2 = 0;
            subida.sector3 = cantidad[j];
            console.log('entro-III-'+i);
          }
          salida.push(subida);
        }
      } else {
        const subida = { incidencia: null, sector1: null, sector2: null, sector3: null };
          if (sect === 'I') {
            subida.incidencia = incidencia[i];
            subida.sector1 = cantidad[i];
            subida.sector2 = 0;
            subida.sector3 = 0;
          } else if (sect === 'II') {
            subida.incidencia = incidencia[i];
            subida.sector1 = 0;
            subida.sector2 = cantidad[i];
            subida.sector3 = 0;
          } else if (sect === 'III') {
            subida.incidencia = incidencia[i];
            subida.sector1 = 0;
            subida.sector2 = 0;
            subida.sector3 = cantidad[i];
            console.log('entro-III-'+i);
          }
          salida.push(subida);
      }
    }
  }
  console.log(salida);

  const medio = this.juntar(salida);
  console.log(medio);
    return medio;
}
  juntar(entrada: Array<ReporteModel>) {
    let i = 0;
    let k = 0;
    const salida = [ { incidencia: null, sector1: null, sector2: null, sector3: null }];
    do {
      if(i===0){
        salida[0]=entrada[0];
        this.labeles[0] = entrada[0].incidencia;
        this.sectores1[0] = entrada[0].sector1;
        this.sectores2[0] = entrada[0].sector2;
        this.sectores3[0] = entrada[0].sector3;
        i++;
        // k++;
      }
      if(entrada[i].incidencia !== salida[k].incidencia){
        console.log('i'+i);
        const medio = { incidencia: null, sector1: null, sector2: null, sector3: null };
        medio.incidencia = entrada[i].incidencia;
        this.labeles.push(medio.incidencia);
        medio.sector1 = entrada[i].sector1;
        this.sectores1.push(medio.sector1);
        medio.sector2 = entrada[i].sector2;
        this.sectores2.push(medio.sector2);
        medio.sector3 = entrada[i].sector3;
        this.sectores3.push(medio.sector3);
        salida.push(medio);
        i++;
        k++;
      } else {
        this.sectores1[k] = salida[k].sector1 = this.mayor(entrada[i].sector1, entrada[k].sector1);
        this.sectores2[k] = salida[k].sector2 = this.mayor(entrada[i].sector2, entrada[k].sector2);
        this.sectores3[k] = salida[k].sector3 = this.mayor(entrada[i].sector3, entrada[k].sector3);
        i++;
      }
    } while (i < entrada.length);
    return salida;
  }

  mayor(a: number, b: number){
    if(a < b){
      return b;
    } else if(a>b){
      return a;
    } else {
      return a;
    }
  }

  createPDF() {
    const sTable = document.getElementById('imprimirreporte').innerHTML;
    let style = '<style>';
    style = style + 'div.pageA4 {font-family: Tahoma, Geneva, Verdana, sans-serif; width: 210mm; height: 245mm;}';
    style = style + 'table td , table th {padding: 3px 2px;}';
    style = style + 'table thead { background: #000000;}';
    style = style + 'table thead th { font-weight: bold;color: #FFFFFF;text-align: center;}';
    style = style + 'table, th, td {border: solid 1px black; border-collapse: collapse;text-align: center;}';
    style = style + 'table tr:nth-child(even){ background: #AFA5AD;}';
    style = style + 'td.incidencia {text-align: left;}';
    style = style + '</style>';
    // CREATE A WINDOW OBJECT.
    const win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>imprimir</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
  exportexcel(): void {
    /* table id is passed over here */
    const element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'INCIDENCIAS.xlsx');

  }



}

export class ResultadoModel {
  data: any = [];
  result: any = [];
}
