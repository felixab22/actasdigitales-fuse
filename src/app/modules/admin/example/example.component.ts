/* eslint-disable @typescript-eslint/type-annotation-spacing */
import {
    Component,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    OnInit,
} from '@angular/core';
import { ParteModel } from '../../../model/incidencia.model';
import { Router } from '@angular/router';
import { IncidenciasService } from 'app/service/incidencias.service';
declare let swal:any;
@Component({
    selector: 'example',
    templateUrl: './example.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleComponent implements OnInit {
    enviar = 'http://localhost:8080/file/subida,{Content-Type: multipart/form-data;}';
    imagen = './assets/img/logo.jpg' || 'surveillance/assets/img/logo.jpg';
    nombre='incidencia-003';
    mostrar=false;
    // eslint-disable-next-line quotes
    link=`watana:?url_logo=http://localhost:8080/file/descarga/logo.png&url_descarga=http://localhost/source/docs/incidencia-002.pdf&url_subida=http://localhost/source/subir/subir.php?name=${this.nombre}.pdf`;
    tern = '';
    listFilter = new Array<ParteModel>();
    // lugarlista = new Array<ParteModel>();
    onelist = new Array<ParteModel>();
    listTemp = new Array<ParteModel>();
    categories = [
        { id: 1, slug: 'I', title: 'Sector I' },
        { id: 2, slug: 'II', title: 'Sector II' },
        { id: 3, slug: 'III', title: 'Sector III' },
    ];
    turno = [
        { id: 1, slug: 'Dia', title: 'Dia' },
        { id: 2, slug: 'Tarde', title: 'Tarde' },
        { id: 3, slug: 'Noche', title: 'Noche' },
    ];

    /**
     * Constructor
     */
     constructor(
        private _router: Router,
        private _incidenciaSrv: IncidenciasService,
    ) {
    }
    ngOnInit(): void {
        this.listaIncidencia();
    }
    // eslint-disable-next-line @angular-eslint/use-lifecycle-interface
    ngAfterContentInit(): void {
        this.mostrar = true;
        //Called after ngOnInit when the component's or directive's content has been initialized.
        //Add 'implements AfterContentInit' to the class.
    }
    filterByCategory(change) {}

    filterListTipo(tipo: any) {
        // console.log(tipo);
        if (tipo === 'all') {
            this.onelist = this.listTemp;
            this.listFilter = this.listTemp;
        } else {
            this.onelist = this.listTemp.filter(course => course.sector === tipo);
            this.listFilter = [...this.onelist];
        }
    }
    generarDocumento(valor){
        const guarda = {tipo:'editar', dato:valor};
        localStorage.setItem('parte',JSON.stringify(guarda));
        this._router.navigate(['/example/Imprimir']);
      }
      listaIncidencia(){
          this._incidenciaSrv.getListaAllParte().subscribe((res:any)=> {
              this.listTemp = this.onelist = this.listFilter = res.data.content;
          });
      }
      deleteIncidencia(valor) {
          if (confirm('¿Está seguro que desea eliminar ?')) {
            this._incidenciaSrv.deleteIncidencia(valor.idparte);
           swal('ELIMINADO!', ' ELIMINADA!');
           this.listFilter = this.listFilter.filter(u => valor.idincidencia !== u.idincidencia);
      }
    }
}
// - en la lista mostar un boton de ver pdf firmado - descargarlo
// - subir la firma mas arriba
