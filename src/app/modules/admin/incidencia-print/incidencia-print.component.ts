/* eslint-disable quotes */
import { Component, OnInit } from '@angular/core';
import { ParteModel, PersonaModel } from 'app/model/incidencia.model';
import { IncidenciasService } from 'app/service/incidencias.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-incidencia-print',
  templateUrl: './incidencia-print.component.html',
  styleUrls: ['./incidencia-print.component.scss']
})
export class IncidenciaPrintComponent implements OnInit {
  imagen = './assets/img/pat1.jpg' || 'surveillance/assets/img/pat1.jpg';
  imprimir = 'watana:?url_logo=http://localhost:8081/file/descarga/logo.png&url_descarga=http://localhost:8081/source/docs/actaprint.pdf&url_subida=http://localhost:8081/source/subir/subir.php?name=inicidencia.pdf&firma_pagina=1&firma_x=150&firma_y=80&firma_largo=300&firma_alto=100&firma_texto=%3CFIRMANTE%3E%0A%3CORGANIZACION%3E%0AFecha%3A%20%3CFECHA%3E%0A%0AFirmado%20en%3A%20Watana.com';
  hoy = new Date();
  parte = new ParteModel();
  persona = new PersonaModel();
  mostarEtqueta = false;
  constructor(
    private _incidenciaSrv: IncidenciasService,
    private sanitizer: DomSanitizer
  ) {
    if (localStorage.getItem('construir') !== null) {
      this.persona = JSON.parse(localStorage.getItem('construir')).idpersona;
    } else {
      this.persona = JSON.parse(localStorage.getItem('personal'));
    }
    this.sanitizer.bypassSecurityTrustUrl(this.imprimir);
    const respuesta = JSON.parse(localStorage.getItem('parte'));
    // console.log(respuesta);
    this.parte.contenido = respuesta.dato.contenido;
    this.parte.nroparte = respuesta.dato.nroparte;
    this.parte.receptor = respuesta.dato.receptor;
    this.parte.referencia = respuesta.dato.referencia;
    this.parte.turno = respuesta.dato.turno;
    this.parte.sector = respuesta.dato.sector;
    this.parte.hora = respuesta.dato.hora;
    this.parte.fname = respuesta.dato.fname;
    this.parte.fregistro = respuesta.dato.fregistro;

    if(respuesta.tipo==='nuevo'){
      this.parte.incidencia = respuesta.dato.incidencia;
      this.parte.idincidencia = respuesta.idparte;
    }
    if(respuesta.tipo==='editar'){
      this.parte.incidencia = respuesta.dato.incidencia.descripcion;
      this.parte.idincidencia = respuesta.dato.idparte;
  }
   }

  ngOnInit(): void {
    console.log(this.parte);
    this.imprimir = `watana:?url_logo=http://localhost:8081/file/descarga/logo.png&url_descarga=http://localhost:8081/source/docs/actaprint.pdf&url_subida=http://localhost:8081/source/subir/subir.php?name=${this.parte.nroparte}-inicidencia.pdf&firma_pagina=1&firma_x=150&firma_y=80&firma_largo=300&firma_alto=100&firma_texto=%3CFIRMANTE%3E%0A%3CORGANIZACION%3E%0AFecha%3A%20%3CFECHA%3E%0A%0AFirmado%20en%3A%20Watana.com`;

  }
  sanitizeImageUrl(imageUrl: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(imageUrl);
}
  firmarDocumento(){
    console.log('firmado');
    this._incidenciaSrv.getFirmarDocumento(this.parte.idincidencia).subscribe((res: any)=> {
      if(res.code === 200){
        this.mostarEtqueta = true;
      }
    });
  }
  createPDF() {
    // eslint-disable-next-line no-var
    var sTable = document.getElementById('imprimirpfg').innerHTML;
    let style = '<style>';
    style = style + 'div.pageA4 {font-family: Tahoma, Geneva, Verdana, sans-serif; width: 210mm; height: 245mm;}';
    style = style + 'p { font-size: 2vw; }';
    style = style + '.cabezera { background-image: url(\'./../../../assets/img/pat1.jpg\')!important; height: 155px;}';
    // style = style + ".raya {text-decoration: underline; }";
    style = style + '.fuerte { font-weight: 700; }';
    style = style + '.izquierda {  margin-left: 8vw; }';
    style = style + '.cuerpo {text-align: justify;}';
    style = style + '.centro { font-size: 10px; display: grid;  grid-template-columns: repeat(3, 1fr); grid-auto-rows: 200px; text-align: center; grid-template-areas: \'a  b  c\' \'d  e  f\'}';
    style = style + '.firma { grid-area: b;  border-bottom: 2px solid black;}';
    style = style + '.nombrs { grid-area: e; font-size: 11px; margin:0; padding:0;}';
    style = style + '</style>';
    // CREATE A WINDOW OBJECT.
    // eslint-disable-next-line no-var
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>imprimir</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
}
