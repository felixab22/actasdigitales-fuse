import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidenciaPrintComponent } from './incidencia-print.component';

describe('IncidenciaPrintComponent', () => {
  let component: IncidenciaPrintComponent;
  let fixture: ComponentFixture<IncidenciaPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncidenciaPrintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidenciaPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
