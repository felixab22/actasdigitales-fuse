import { Component, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UsuariosComponent {
  formFieldHelpers: string[] = [''];

  constructor(private _formBuilder: FormBuilder) {}


  getFormFieldHelpersAsString(): string
  {
      return this.formFieldHelpers.join(' ');
  }
}
