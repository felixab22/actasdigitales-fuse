export class ParteModel {
    nroparte?: string;
    receptor?: string;
    referencia?: string;
    turno: string;
    sector: string;
    contenido: string;
    incidencia?: any;
    idpersona?: any;
    idparte?: any;
    hora?: any;
    fecha?: any;
    fname?: string;
    fregistro?: string;
    idincidencia?: any;
}
export class PersonaModel {
    idpersona: number;
    nombre: string;
    apellido: string;
    dni: string;
    cargo: string;
}

export class IncidenciaModel {
    idincidencia: number;
    descripcion: string;
}

export class ReporteModel {
    incidencia: string;
    sector1: number;
    sector2: number;
    sector3: number;
}
